import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  output = 'Output';

  principal = 'Principal';

  votantes = ['Marcos','Paulo','Lopes'];

  concordou = 0;
  naoConcordou = 0;

  foiVotado(concordo : boolean){
    concordo ? this.concordou++ : this.naoConcordou++;
  }

  life = "Gancho de Vida";
  
}
