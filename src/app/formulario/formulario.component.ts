import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {
  title = 'Formulario';

  corFavoritaControl = new FormControl('azul');
  corFavorita = 'Vermelho';

  constructor() { }

  ngOnInit() {
  }

}
