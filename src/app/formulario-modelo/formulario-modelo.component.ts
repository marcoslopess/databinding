import { Component, OnInit } from '@angular/core';
import { Pessoa } from '../pessoa';

@Component({
  selector: 'app-formulario-modelo',
  templateUrl: './formulario-modelo.component.html',
  styleUrls: ['./formulario-modelo.component.css']
})
export class FormularioModeloComponent implements OnInit {

  pessoa = new Pessoa(1,'marcos',23,'masculino');

  enviado = false;

  enviar(){
    this.enviado = true;
  }

  get diagnostico(){
    return JSON.stringify(this.pessoa);
  }

  novaPessoa(){
    this.pessoa = new Pessoa(2,'lopes', 22, 'masculino');
  }

  constructor() { }

  ngOnInit() {
  }

}
