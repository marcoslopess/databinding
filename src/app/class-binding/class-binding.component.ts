import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-class-binding',
  templateUrl: './class-binding.component.html',
  styleUrls: ['./class-binding.component.css']
})
export class ClassBindingComponent implements OnInit {
  title = 'Class e Style Binding';
  public aplicarClasse = true;
  public aplicarStyle = true;

  setarStyle(){
    let styles = {
      'background-color': this.aplicarStyle ? 'red' : 'transparent',
      'font-weight': this.aplicarStyle ?'bold' : 'normal'
    }
    return styles;
  }

  constructor() { }

  ngOnInit() {
  }

}
