import { FormControl } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario-reativo',
  templateUrl: './formulario-reativo.component.html',
  styleUrls: ['./formulario-reativo.component.css']
})
export class FormularioReativoComponent implements OnInit {
  title = 'Formulario Reativo';

  nome = new FormControl('Angular');

  atualizarNome(){
    this.nome.setValue('Marcos');
  }

  constructor() { }

  ngOnInit() {
  }

}
