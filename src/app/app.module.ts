import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PropertybindingComponent } from './propertybinding/propertybinding.component';
import { InterpolacaoComponent } from './interpolacao/interpolacao.component';
import { EventbindingComponent } from './eventbinding/eventbinding.component';
import { TwoWayDatabindingComponent } from './two-way-databinding/two-way-databinding.component';
import { ClassBindingComponent } from './class-binding/class-binding.component';
import { InputComponent } from './input/input.component';
import { OutputComponent } from './output/output.component';
import { NgIfComponent } from './ng-if/ng-if.component';
import { NgForComponent } from './ng-for/ng-for.component';
import { NgSwitchComponent } from './ng-switch/ng-switch.component';
import { LifeCycleComponent } from './life-cycle/life-cycle.component';
import { FormularioComponent } from './formulario/formulario.component';
import { from } from 'rxjs';
import { FormularioReativoComponent } from './formulario-reativo/formulario-reativo.component';
import { FormularioReativoGrupoComponent } from './formulario-reativo-grupo/formulario-reativo-grupo.component';
import { FormularioReativoGrupoAninhadoComponent } from './formulario-reativo-grupo-aninhado/formulario-reativo-grupo-aninhado.component';
import { FormularioModeloComponent } from './formulario-modelo/formulario-modelo.component';

@NgModule({
  declarations: [
    AppComponent,
    PropertybindingComponent,
    InterpolacaoComponent,
    EventbindingComponent,
    TwoWayDatabindingComponent,
    ClassBindingComponent,
    InputComponent,
    OutputComponent,
    NgIfComponent,
    NgForComponent,
    NgSwitchComponent,
    LifeCycleComponent,
    FormularioComponent,
    FormularioReativoComponent,
    FormularioReativoGrupoComponent,
    FormularioReativoGrupoAninhadoComponent,
    FormularioModeloComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule, 
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
