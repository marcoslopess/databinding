import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interpolacao',
  templateUrl: './interpolacao.component.html',
  styleUrls: ['./interpolacao.component.css']
})
export class InterpolacaoComponent implements OnInit {
  title = 'Data Binding';
  curso = 'Angular 6'

  constructor() { }

  ngOnInit() {
  }

}
