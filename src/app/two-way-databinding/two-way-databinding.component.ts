import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-two-way-databinding',
  templateUrl: './two-way-databinding.component.html',
  styleUrls: ['./two-way-databinding.component.css']
})
export class TwoWayDatabindingComponent implements OnInit {
  title = 'Two Way Data Binding';
  nomeCurso = 'Angular 6'

  constructor() { }

  ngOnInit() {
  }

}
