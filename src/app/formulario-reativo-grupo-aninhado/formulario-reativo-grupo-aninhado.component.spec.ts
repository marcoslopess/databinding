import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioReativoGrupoAninhadoComponent } from './formulario-reativo-grupo-aninhado.component';

describe('FormularioReativoGrupoAninhadoComponent', () => {
  let component: FormularioReativoGrupoAninhadoComponent;
  let fixture: ComponentFixture<FormularioReativoGrupoAninhadoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioReativoGrupoAninhadoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioReativoGrupoAninhadoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
