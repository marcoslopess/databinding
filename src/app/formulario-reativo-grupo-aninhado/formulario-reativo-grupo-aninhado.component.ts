import { FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-formulario-reativo-grupo-aninhado',
  templateUrl: './formulario-reativo-grupo-aninhado.component.html',
  styleUrls: ['./formulario-reativo-grupo-aninhado.component.css']
})
export class FormularioReativoGrupoAninhadoComponent implements OnInit {
  title = 'Formulario Reativo Grupo Aninhado';

  // dadosPessoais = new FormGroup({
  //     nome: new FormControl(''),
  //     sobrenome: new FormControl(''),
  //     endereco: new FormGroup({
  //         rua: new FormControl(''),
  //         cidade: new FormControl(''),
  //         estado: new FormControl(''),
  //         cep: new FormControl('')
  //       })
  //   });

    enviarValores(){
      console.warn(this.dadosPessoais.value);
    }

  constructor(private fb: FormBuilder) { }

  dadosPessoais = this.fb.group({
    nome: ['', Validators.required],
    sobrenome: ['', Validators.required],
    endereco: this.fb.group({
        rua: [''],
        cidade: [''],
        estado: [''],
        cep: [''],
      })
  });

  ngOnInit() {
  }

}
