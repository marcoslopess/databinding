import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-switch',
  templateUrl: './ng-switch.component.html',
  styleUrls: ['./ng-switch.component.css']
})
export class NgSwitchComponent implements OnInit {
  title = 'NgSwitch';

  selection = 'Angular';
  options = ['Angular','React','Vue',''];

  constructor() { }

  ngOnInit() {
  }

}
