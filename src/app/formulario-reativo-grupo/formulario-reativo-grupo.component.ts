import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-formulario-reativo-grupo',
  templateUrl: './formulario-reativo-grupo.component.html',
  styleUrls: ['./formulario-reativo-grupo.component.css']
})
export class FormularioReativoGrupoComponent implements OnInit {
  title = 'Formulario Reativo Grupo';

  constructor() { }

  pessoaForm = new FormGroup(
    {
      primeiroNome: new FormControl(''),
      ultimoNome: new FormControl('')
    }
  );

  enviarValores(){
    console.warn(this.pessoaForm.value);
  }
  ngOnInit() {
  }

}
