import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioReativoGrupoComponent } from './formulario-reativo-grupo.component';

describe('FormularioReativoGrupoComponent', () => {
  let component: FormularioReativoGrupoComponent;
  let fixture: ComponentFixture<FormularioReativoGrupoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioReativoGrupoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioReativoGrupoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
