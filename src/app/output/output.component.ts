import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-output',
  templateUrl: './output.component.html',
  styleUrls: ['./output.component.css']
})
export class OutputComponent implements OnInit {

  @Output() enviarVoto = new EventEmitter<boolean>();
  foiVotado = false

  vote(concordo: boolean){
    this.enviarVoto.emit(concordo);
    this.foiVotado = true;
  }

  constructor() { }

  ngOnInit() {
  }

}
